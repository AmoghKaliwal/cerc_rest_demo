# importing module
import os
import constants
import text_from_video_audio
import text_from_docs
import face_features

""" Video Files """
# India's Harish Salve argues case for Kulbhushan Jadhav at ICJ
video_filename = "Harish Salve.mp4"

# function to extract video to text data
text_from_video_audio.video_audio_to_text(
        FilePath = os.path.join(constants.base_path, 
                                constants.video_folder, 
                                video_filename),
        FileType = "video")

""" Audio Files """
# Lecture of Mr. Justice R.F. Nariman.wav
audio_filename = "Justice Nariman.wav"

# function to extract audio to text data
text_from_video_audio.video_audio_to_text(
        FilePath = os.path.join(constants.base_path, 
                                constants.audio_folder, 
                                audio_filename),
        FileType = "audio")

""" Documents """
# Petition for revision of transmission tariff
pdf_filename = "339-TT-2019-24-08.pdf"

# function to extract text from pdf
text_from_docs.pdf_to_text(
        FilePath = os.path.join(constants.base_path, 
                                constants.docs_folder, 
                                pdf_filename))

""" Image Files """
# India's Harish Salve argues case for Kulbhushan Jadhav at ICJ
image_filename = "Harsh_Salve.jpg"

# function to detect face
face_features.identify_face(
        ImageFilePath = os.path.join(constants.base_path, 
                                     constants.image_folder, 
                                     image_filename),
        VideoFilePath = os.path.join(constants.base_path, 
                                     constants.video_folder, 
                                     video_filename))